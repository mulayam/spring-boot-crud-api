package com.example.demo.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*", methods = { RequestMethod.DELETE,
		RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT })
@RequestMapping("/students")
public class StudentController {

	@GetMapping
	public List<Student> getStudent() {
		return StudentService.STUDENT_LIST;
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<ApiResponse<String>> delete(@PathVariable int id) {
		int index = StudentService.STUDENT_LIST.indexOf(new Student(id));
		if (index != -1) {
			StudentService.STUDENT_LIST.remove(index);
			return ResponseEntity.ok(new ApiResponse<String>("Deleted", null));

		} else {
			return ResponseEntity.ok(new ApiResponse<String>(id + " Not found", null));
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<ApiResponse<Student>> getStudent(@PathVariable int id) {
		int index = StudentService.STUDENT_LIST.indexOf(new Student(id));
		if (index != -1) {
			Student s = StudentService.STUDENT_LIST.get(index);

			return ResponseEntity.ok(new ApiResponse<Student>("SUCCESS", s));

		} else {
			return ResponseEntity.ok(new ApiResponse<Student>("SUCCESS", null));
		}
	}

	@PostMapping
	public ResponseEntity<ApiResponse<Student>> createStudent(@RequestBody Student student) {
		int nextId = StudentService.nextId++;
		student.setId(nextId);
		StudentService.STUDENT_LIST.add(student);
		return ResponseEntity.ok(new ApiResponse<Student>(student));
	}

	@PutMapping
	public ResponseEntity<ApiResponse<Student>> editStudent(@RequestBody Student student) {
		int index = StudentService.STUDENT_LIST.indexOf(student);
		if (index != -1) {
			StudentService.STUDENT_LIST.set(index, student);
		}

		return ResponseEntity.ok(new ApiResponse<Student>(student));

	}
}
