package com.example.demo.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(exclude = { "name", "phone", "address", "age" })
public class Student {
	private int id;
	private String name;
	private String phone;
	private String address;
	private int age;

	public Student(int id) {
		super();
		this.id = id;
	}

}
