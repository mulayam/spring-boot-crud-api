package com.example.demo.controller;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
public class ApiResponse<T> {

	private String message = "SUCCESS";
	private T data;

	public ApiResponse(String message, T data) {
		super();
		this.message = message;
		this.data = data;
	}

	public ApiResponse(T data) {
		super();
		this.data = data;
	}

	public ApiResponse(String message) {
		super();
		this.message = message;
	}

}
